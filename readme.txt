Hey guys,

please see my solution to test assignment attached.

Solution is pretty straightforward, I've placed comments in CSS and JS files too.

Few comments here:

- I've used CodeKit to compile styles (SCSS) and scripts. Both source and min files are attached.

- Any number of symbols can be added in settings variable that is located in scripts

- To add new symbol to the list someone should add corresponding id, class, name and winning positions and values. To add an image someone should add corresponding class in CSS and link it to new image in /assets/images folder.

- Image can be of any size and proportion, it will fit into symbol area. For this reason I haven't compiled images into one sprite - symbols are very easy to add this way. When app goes into production - images can be compiled into one sprite.

- Combination prizes can be specified in the settings too. Please follow an existing example.

- This wasn't clearly specified in the task, so I've set app to detect and sum up multiple wins on top and bottom lines. App can be set of course to detect only one win in specific order - top before bottom line, prize sum or any other specific prize order.

- All info regarding symbols, wins, prizes and current balance is stored and evaluated in JS, html is only receiving data and representing results.

- I haven't used any library/framework just native JS and SCSS.

- App is responsive, sort of. Doesn't look fantastic on different resolutions but works on resolutions as small as 320vw. Any height is supported too, of cause.

- According to specifications app should work in Chrome - that is done. I haven' looked it in other browsers - however if required, I can make this or any other my app work similarly in all browsers. In my current company we are supporting all browsers that have usage share over 2%. That means that our websites should work identically in all of them.

- Overall app is done to minimal specifications and I haven't added any additional features that expose my skills for following reasons. There is too much of different things that could be done to make this app look better or be better prepared to be launched to production. Even if I tried hard - I couldn't do all that I potentially can over span of a week, and also I had a very busy time at my present work sadly. 
  However, I can, and I'll gladly do any additional specific request or update to this app if needed, so please ask.

- I was forced to work on this app several times during the week, so it was hard to keep track of hours, but approximately I've spent 16-18 hours total.
