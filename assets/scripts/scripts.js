'use strict';


/* Base settings, symbols can be added and any combination prizes are allowed */

const symbols = new Map([
    ["3xbar", {
        id:"3xbar",
        name: "3xBAR",
        class: "bar3x",
        prize: {
            topLine : 50,
            centerLine : 50,
            bottomLine : 50
        }
    }],
    ["bar", {
        id: "bar",
        name: "BAR",
        class: "bar",
        prize: {
            topLine : 20,
            centerLine : 20,
            bottomLine : 20
        }
    }],
    ["2xbar", {
        id: "2xbar",
        name: "2xBAR",
        class: "bar2x",
        prize: {
            topLine : 10,
            centerLine : 10,
            bottomLine : 10
        }
    }],
    ["seven", {
        id: "seven",
        name: "7",
        class: "seven",
        prize: {
            topLine : 150,
            centerLine : 150,
            bottomLine : 150
        }
    }],
    ["cherry", {
        id: "cherry",
        name: "CHERRY",
        class: "cherry",
        prize: {
            topLine : 2000,
            centerLine : 1000,
            bottomLine : 4000
        }
    }]
]);

const settings = {
    symbols: symbols,
    combinationPrizes: [
        {
            names:["seven", "cherry"],
            prize: {
                topLine : 75,
                centerLine : 75,
                bottomLine : 75
            }
        },
        {
            names:["3xbar", "bar", "2xbar"],
            prize: {
                topLine : 5,
                centerLine : 5,
                bottomLine : 5
            }
        }
    ],
    startingBalance: 1000
};





/* Declaring variables */

const balanceInput = document.getElementById('balance'),
    spinButton = document.getElementById('spin-button'),
    reelsWrapper = document.getElementById('reels'),
    reels = {
        reel1 : document.querySelector(".reel[data-id='1']"),
        reel2 : document.querySelector(".reel[data-id='2']"),
        reel3 : document.querySelector(".reel[data-id='3']")
    },
    debuggingReels = {
        reel1 : {
            symbol : document.querySelector(".debugging-symbol[data-id='1']"),
            position: document.querySelector(".debugging-position[data-id='1']")
        },
        reel2 : {
            symbol : document.querySelector(".debugging-symbol[data-id='2']"),
            position: document.querySelector(".debugging-position[data-id='2']")
        },
        reel3 : {
            symbol : document.querySelector(".debugging-symbol[data-id='3']"),
            position: document.querySelector(".debugging-position[data-id='3']")
        }
    },
    debuggingOptions = document.getElementById("debugging-options"),
    debuggingModeControl = document.getElementById("debugging-mode"),
    payout = document.getElementById('payout'),
    rotationResult = {
        reel1 : {
            topLine : false,
            centerLine : false,
            bottomLine : false
        },
        reel2 : {
            topLine : false,
            centerLine : false,
            bottomLine : false
        },
        reel3 : {
            topLine : false,
            centerLine : false,
            bottomLine : false
        }
    },
    currentPrizes = {
        topLine: [],
        centerLine: [],
        bottomLine: []
    },
    symbolsArray = Array.from(settings.symbols.keys()),
    symbolAngle = 360 / settings.symbols.size,
    zOffset = (reels.reel1.offsetHeight * 0.52 * settings.symbols.size) / (2 * Math.PI);

let rotating = false,
    currentBalance = settings.startingBalance,
    mode = "random";





/* Populating html with data */

let iterator = 0;
for (let [key, symbol] of settings.symbols) {

    let tempElement = document.createElement("div");
    tempElement.classList.add('symbol', symbol.class);
    tempElement.setAttribute("id", symbol.id);

    tempElement.style.transform = 'rotateX(' + (-symbolAngle * iterator) + 'deg) translateZ(' + zOffset + 'px)';

    for (let reel in reels) {

        if (reels.hasOwnProperty(reel)) {
            reels[reel].querySelector('.reel-inner-wrapper').appendChild(tempElement.cloneNode());
        }
    }

    for (let reel in debuggingReels) {

        if (debuggingReels.hasOwnProperty(reel)) {
            debuggingReels[reel].symbol.options[debuggingReels[reel].symbol.options.length] = new Option(symbol.name, symbol.id);
        }
    }
    iterator++;
}

balanceInput.value = settings.startingBalance;




/* Modifying balance in input */

balanceInput.addEventListener('change', (event) => {
    let entered = Number.parseInt(balanceInput.value);
    if(entered > 1) {
        if(entered < 5000) {
            currentBalance = entered;
        }else {
            currentBalance = 5000;
            balanceInput.value = 5000;
        }
    }else {
        currentBalance = 1;
        balanceInput.value = 1;
    }
});



/* Calculate reel full result from set symbol and line */

const setReelResult = (reel, symbol, position) => {

    if(position === "centerLine"){

        rotationResult[reel].topLine = false;
        rotationResult[reel].centerLine = symbol;
        rotationResult[reel].bottomLine = false;

    }else {
        rotationResult[reel].centerLine = false;

        let symbolsArray = Array.from(settings.symbols.keys());

        if(position === "topLine") {
            rotationResult[reel].topLine = symbol;

            if(symbolsArray[symbolsArray.length - 1] === symbol){

                rotationResult[reel].bottomLine = symbolsArray[0];

            }else {
                rotationResult[reel].bottomLine = symbolsArray[symbolsArray.findIndex(position => position === symbol) + 1];
            }
        }
        if(position === "bottomLine") {
            rotationResult[reel].bottomLine = symbol;

            if(symbolsArray[0] === symbol){
                rotationResult[reel].topLine = symbolsArray[symbolsArray.length - 1];
            }else {
                rotationResult[reel].topLine = symbolsArray[symbolsArray.findIndex(position => position === symbol) - 1];
            }
        }
    }
};




/* Rotate and set reels to result positions, clear prize data and flash results */

const rotateReel = () => {
    let iterate = 0;
    for (let reel in reels) {
        if (reels.hasOwnProperty(reel)) {
            let currentReelPosition;
            if(rotationResult[reel].topLine !== false) {
                currentReelPosition = symbolsArray.findIndex(position => position === rotationResult[reel].topLine) * symbolAngle + symbolAngle / 2;
            }else {
                currentReelPosition = symbolsArray.findIndex(position => position === rotationResult[reel].centerLine) * symbolAngle;
            }

            reels[reel].querySelector('.reel-inner-wrapper').style.transition = 'transform ' + (2 + 0.5 * iterate) + 's';
            reels[reel].querySelector('.reel-inner-wrapper').style.transform = 'rotateX(' + (currentReelPosition + 360 * 20) + 'deg)';
            setTimeout(() => {
                reels[reel].querySelector('.reel-inner-wrapper').style.transition = 'transform 0s';
                reels[reel].querySelector('.reel-inner-wrapper').style.transform = 'rotateX(' + (currentReelPosition) + 'deg)';
            }, 2000 + 500 * iterate);
        }
        iterate ++;
    }

    setTimeout(() => {

        let hasWins = false;

        for (let prizeLine in currentPrizes) {
            if (currentPrizes.hasOwnProperty(prizeLine)) {
                if(currentPrizes[prizeLine].length){
                    hasWins = true;

                    reelsWrapper.classList.add(prizeLine);

                    currentPrizes[prizeLine].forEach((prize) => {
                        currentBalance += prize;
                        balanceInput.value = currentBalance;
                        let tempPrize = document.createElement("span");
                        let textnode = document.createTextNode(prize);
                        tempPrize.appendChild(textnode);
                        payout.appendChild(tempPrize);
                    });
                }
            }
        }

        for (let prizeLine in currentPrizes) {
            if (currentPrizes.hasOwnProperty(prizeLine)) {
                currentPrizes[prizeLine] = [];
            }
        }

        if(hasWins){
            payout.classList.add('flash');
        }

        rotating = false;

    }, 3100);
};




/* Spin button click even. */

spinButton.addEventListener('click', (event) => {


    /* Remove win messages */
    payout.classList.remove('flash');

    reelsWrapper.classList.remove('topLine', "centerLine", "bottomLine");

    while (payout.firstChild) {
        payout.removeChild(payout.firstChild);
    }


    /* Ensure reels are not spinning already and balance is positive */
    if(rotating === true || currentBalance < 1) {
        return false;
    }

    rotating = true;

    balanceInput.value = --currentBalance;


    /* Selecting mode */
    mode = debuggingModeControl[debuggingModeControl.selectedIndex].value;

    if(mode === "random") {
        let positionArray = ["topLine", "centerLine", "bottomLine"];

        /* Storing symbols positions in the variable */
        for (let reel in reels) {
            if (reels.hasOwnProperty(reel)) {
                setReelResult(reel, symbolsArray[Math.floor(Math.random() * symbolsArray.length)], positionArray[Math.floor(Math.random() * positionArray.length)]);
            }
        }

    }else {
        for (let reel in debuggingReels) {
            if (debuggingReels.hasOwnProperty(reel)) {
                setReelResult(reel, debuggingReels[reel].symbol[debuggingReels[reel].symbol.selectedIndex].value, debuggingReels[reel].position[debuggingReels[reel].position.selectedIndex].value);
            }
        }
    }




    /* Defining winning combinations */

    for (let prizeLine in currentPrizes) {
        if (currentPrizes.hasOwnProperty(prizeLine)) {
            let uniqueLineResults = [];

            for (let reel in rotationResult) {
                if (rotationResult.hasOwnProperty(reel)) {
                    if(!uniqueLineResults.includes(rotationResult[reel][prizeLine])) {
                        uniqueLineResults.push(rotationResult[reel][prizeLine]);
                    }
                }
            }

            if(!uniqueLineResults.includes(false)){
                if(uniqueLineResults.length === 1) {

                    currentPrizes[prizeLine].push(settings.symbols.get(uniqueLineResults[0]).prize[prizeLine]);

                }else{
                    let currentCombinationPrizes = [];

                    settings.combinationPrizes.forEach((combinationPrize) => {
                        let hasPrize = true;
                        uniqueLineResults.forEach((prize) => {
                            if(!combinationPrize["names"].includes(prize)){

                                hasPrize = false;
                                return false;
                            }
                        });

                        if(hasPrize){
                            currentCombinationPrizes.push(combinationPrize["prize"][prizeLine]);
                        }
                    });

                    currentCombinationPrizes.forEach((currentCombinationPrize) => {
                        currentPrizes[prizeLine].push(currentCombinationPrize);
                    });

                }
            }
        }
    }


    /* Rotate and set reels to determined positions */
    rotateReel();
});